package com.expedia.bcd.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TrendsController {

    private final TrendsServices trendsServices;

    @Autowired
    public TrendsController(TrendsServices trendsServices) {
        this.trendsServices = trendsServices;
    }

    @CrossOrigin
    @RequestMapping(value = "/getTrends", method = RequestMethod.GET, produces = "application/json")
    public String getTrends(@RequestParam(name = "platform", required = false, defaultValue = "expedia") String platform) throws JsonProcessingException {

        trendsServices.insertData(platform);

        if (platform.equals("expedia") ){
            return trendsServices.getExpediaTrends();
        }else if ( platform.equals("google")){
            return trendsServices.getGoogleTrends();
        }else  {
            return trendsServices.getErrorPage();
        }
    }
}

