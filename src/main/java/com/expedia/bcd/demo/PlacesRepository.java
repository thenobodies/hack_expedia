package com.expedia.bcd.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PlacesRepository extends MongoRepository<Place,String> {
    List<Place> findByPlatform(String platform);
}
