package com.expedia.bcd.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class TrendsServices {

    private static final Logger logger = LoggerFactory.getLogger(TrendsServices.class);
    private final PlacesRepository placesRepository;

    @Autowired
    public TrendsServices(PlacesRepository placesRepository) {
        this.placesRepository = placesRepository;
    }

    void insertData(String platform) {
        int id=1;
        int bookingsCount=0;
        String[] location = {"Hong Kong", "Singapore", "Bangkok", "London", "Dubai", "Paris", "Frankfurt", "Geneva", "Amsterdam", "Chicago", "New York", "Las Vegas","Tokyo", "Macau",
                "Sydney", "Cape Town", "Washington DC", "Madrid", "Delhi", "Barcelona", "Brussels", "Mumbai", "Moscow", "Rio de Janeiro", "Buenos Aires"};
        Random random = new Random();

        for( String place: location) {
            placesRepository.save(new Place(String.valueOf(id++), place, bookingsCount + 1000 + random.nextInt(1000),platform));
        }
    }

    String getExpediaTrends() throws JsonProcessingException {
        List<Place> places = filter( placesRepository.findByPlatform("expedia") );
        places.sort((o1, o2) -> o2.getBookingsCount() - o1.getBookingsCount());
        logger.info("Response:"+places);
        return getJsonResponse(places);
        }

    String getGoogleTrends() throws JsonProcessingException {
        List<Place> places = filter( placesRepository.findByPlatform("google") );
        places.sort((o1, o2) -> o2.getBookingsCount() - o1.getBookingsCount());
        logger.info("Response:"+places);
        return getJsonResponse(places);
    }

    String getErrorPage() {
        return " { \"response\":\"No such platform exists\" } ";
    }

    List<Place> filter(List<Place> all) {
        Random rand = new Random();

        int i = 4 + rand.nextInt(1);
        List<Place> filtered = new ArrayList<>();
        for (int j=i; j<i+10; j++) {
            filtered.add(all.get(j));
        }
        return filtered;
    }

    private String getJsonResponse(List<Place> places) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer();
        logger.info("Response:"+places);
        return ow.writeValueAsString(places);
    }
}