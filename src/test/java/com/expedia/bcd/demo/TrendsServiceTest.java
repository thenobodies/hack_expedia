package com.expedia.bcd.demo;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TrendsServiceTest {

    @Autowired  TrendsServices trendsServices;

    @Test
    public void getExpediaTrends() throws JsonProcessingException {
        Assert.assertNotNull(trendsServices.getExpediaTrends());
    }

    @Test
    public void testFiltered() {
        List<Place > list = new ArrayList<>();
        Assert.assertNotNull( trendsServices.filter( list) );
    }
}
